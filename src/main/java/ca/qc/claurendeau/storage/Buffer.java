package ca.qc.claurendeau.storage;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
    private int capacity;
    private CircularFifoQueue<Element> queue;

    public Buffer(int capacity)
    {
        this.capacity = capacity;
        this.queue = new CircularFifoQueue<Element>(capacity);
    }
    
    public Buffer(int capacity, CircularFifoQueue<Element> queue)
    {
        this.capacity = capacity;
        this.queue = queue;
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
        return "Buffer [capacity=" + capacity + ", queue=" + queue + "]";
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return this.capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        return queue.size();
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        return this.queue.isEmpty();
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        return this.queue.isAtFullCapacity();
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
    	if(this.isFull())
    		throw new BufferFullException();
        this.queue.add(element);
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException
    {
    	if(this.isEmpty())
    		throw new BufferEmptyException();
        return this.queue.poll();
    }
}
