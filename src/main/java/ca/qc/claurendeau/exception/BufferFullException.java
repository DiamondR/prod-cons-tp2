package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
	private static final long serialVersionUID = -6461120633011544374L;
	private static String message = "Le buffer est plein";
	
	public BufferFullException() {
		super(message);
	}

	public BufferFullException(String message) {
		super(message);
	}
}
