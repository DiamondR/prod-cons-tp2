package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {
	
	private static final long serialVersionUID = 5597037709742057295L;
	private static String message = "Le buffer est vide";
	
	public BufferEmptyException() {
		super(message);
	}

	public BufferEmptyException(String message) {
		super(message);
	}
}
