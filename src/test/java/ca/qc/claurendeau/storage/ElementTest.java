package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;
import org.junit.*;

public class ElementTest {
	private Element element;
	private int valeurTest = 24;

	@Before
	public void setup() {
		element = new Element();
	}
    @Test
    public void testSetterGetterForElement() {
    	element.setData(valeurTest);
        assertEquals(valeurTest, element.getData());
    }
}
