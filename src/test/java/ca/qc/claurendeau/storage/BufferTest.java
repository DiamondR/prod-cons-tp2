package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.junit.*;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {

	private Buffer emptyBuffer;
	private Buffer filledBuffer;
	private Buffer fullBuffer;
	private CircularFifoQueue<Element> testQueue;
	private CircularFifoQueue<Element> testFullQueue;
	private int valeurTest = 15;
	private Element elementTest = new Element(valeurTest);
	@Before
	public void setup() {
		emptyBuffer = new Buffer(valeurTest);
		
		testQueue = new CircularFifoQueue<Element>(valeurTest);
		testQueue.add(elementTest);
		filledBuffer = new Buffer(valeurTest, testQueue);
		
		testFullQueue = new CircularFifoQueue<Element>(valeurTest);
		for(int i = 0; i < valeurTest; i++)
			testFullQueue.add(elementTest);
		
		fullBuffer = new Buffer(testQueue.size(), testFullQueue);
	}
    @Test
    public void testBuildSuccess() {
        assertTrue(true);
    }
    
    @Test
    public void testCapacity() {
        assertTrue(emptyBuffer.capacity() == valeurTest);
    }
    
    @Test
    public void testGetCurrentLoad() {
        assertTrue(emptyBuffer.getCurrentLoad() == 0);
    }
    
    @Test 
    public void testAddElement() throws BufferFullException {
    	int defaultSize = emptyBuffer.getCurrentLoad();
    	emptyBuffer.addElement(elementTest);
    	assertTrue(emptyBuffer.getCurrentLoad() == defaultSize+1);
    }
    
    @Test 
    public void testRemoveElement() throws BufferEmptyException {
    	int defaultSize = filledBuffer.getCurrentLoad();
    	Element polledElement = filledBuffer.removeElement();
    	assertTrue(filledBuffer.getCurrentLoad() == defaultSize-1);
    	assertNotNull(polledElement);
    }
    
    @Test
    public void testIsEmpty() {
    	assertTrue(emptyBuffer.isEmpty());
    	assertFalse(filledBuffer.isEmpty());
    }
    
    @Test
    public void testIsFull() {
    	assertTrue(fullBuffer.isFull());
    	assertFalse(emptyBuffer.isFull());
    }
    
    @Test
    public void testToString() {
    	assertNotNull(filledBuffer.toString());
    }
    
    @Test(expected = BufferFullException.class)
    public void testThrowBufferFullException() throws BufferFullException {
    	fullBuffer.addElement(elementTest);
    }
    
    @Test(expected = BufferEmptyException.class)
    public void testThrowBufferEmptyException() throws BufferEmptyException {
    	emptyBuffer.removeElement();
    }

}


